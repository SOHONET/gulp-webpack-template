var path = require("path"),
    webpack = require("webpack"),
    ExtractTextPlugin = require("extract-text-webpack-plugin");

var plugins = [
    new webpack.optimize.CommonsChunkPlugin(
        {
            name: 'commons',
            filename: 'common.js'
        }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
    }),
    new webpack.IgnorePlugin(/src\/libs\/*/),
    new ExtractTextPlugin("[name].css")
];

module.exports = {
    cache: true,
    entry: {
        index: "js/index.js",
        alarm: 'js/alarm.js'
    },
    plugins: plugins,
    output: {
        path: __dirname + '/dist/js',
        filename: "[name].js",
        publicPath: './dist/'
    }
    ,
    module: {
        loaders: [
            {test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader")},
            {test: /\.js$/, loader: "babel"},
            {test: /\.less$/, loader: "style!css!less?sourceMap"},
            {test: /\.(gif|png|jpg|jpeg)$/, loader: "url?limit=8192&name=images/[name].[ext]"},
            {test: /\.(eot|svg|ttf|woff|woff2)$/, loader: "url"}
        ]
    }
    ,
    resolve: {
        root: __dirname,
        extensions: ['', '.js', '.json', '.less'],
        alias: {
            respond: 'js/respond.min.js',
            html5shiv: 'js/html5shiv.min.js',
            jquery: "js/jquery-1.12.1.min.js",
            // jquery: "js/jquery-2.2.1.min.js",
            jqueryUI: "js/jquery-ui.min.js",
            jqFullPage: "js/jquery.fullPage.min.js",
            bootstrapJS: 'js/bootstrap.min.js',
            bootstrapTable: 'js/bootstrap-table.min.js',
            perfectScrollbar: 'js/perfect-scrollbar.jquery.min.js',
            echarts: 'js/echarts.min.js',
            world: 'js/world.js',
            common: 'js/common.js'
        }
    }
    ,
    externals: {
        '$': 'jquery'
    }

};