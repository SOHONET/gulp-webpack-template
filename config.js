var srcDir = './src';
var distDir = './dist';
var tempDir = './.tmp';

module.exports = {
    less: {
        all: srcDir + '/less/**/*.less',    //all less files
        src: srcDir + '/less/*.less',       //neet to be compiled less files
        dist: distDir + 'dist/css',
        setting: {}
    },
    image: {
        src: srcDir + '/img/**/*',
        dist: distDir + '/img'
    },
    js:{
        src: srcDir + '/js/**/*',
        dist: distDir + '/js'
    },
    css:{
        src: srcDir + '/css/**/*',
        dist: distDir + '/css'
    },
    font:{
        src: srcDir + '/font/**/*',
        dist: distDir + '/font'
    },
    html:{
        src: srcDir + '/*.html',
        dist: distDir
    },
    sprite:{
        src: srcDir + '/sprites/**/*.{png,jpg,jpeg}'
    }
};